---
title: Información inicial
summary:
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2021-09-01T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: serif
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: dracula
---
{{< slide background-image="featured.jpg" >}}


# Información inicial 1ºBachillerato

[Departamento de Matemáticas - IES Pedro Cerrada](https://crdguez.gitlab.io/)

---

## Presentación

* Carlos Rodríguez
    - email: rodriguezjasoc@iespedrocerrada.org
    - web: [crdguez.gitlab.io](crdguez.gitlab.io)
* Información --> Web del centro --> Departamentos --> Matemáticas --> Carlos Rodríguez

---

## Programación

---

### Funcionamiento de Curso

* Dos exámenes por trimestre --> Pesos: **20%** el primero y **80%** el segundo
* Primer examen a mitad evaluación (50 minutos) y segundo examen a final de trimestre según calendario de Jefatura (90 minutos)
* Nota de evaluación: Media ponderada de los exámenes

---

### Funcionamiento de Curso

* La 1ª y 2ª evaluación podrán ser recuperadas mediante un examen durante la evaluación siguiente. Este examen será voluntario para quien tuviera la evaluación aprobada.
* Nota provisional al final de la 3ª evaluación: Media de las evaluaciones

---

### Funcionamiento de Curso


* Examen final de curso: Finalizada la 3ª evaluación, se realizará de manera obligatoria el examen final de toda la asignatura. Este examen permite recuperar evaluaciones pendientes o subir la nota media de aquellos alumnos cuya nota provisional fuera superior a cinco (en casos excepcionales  podría bajarla). Aquellos alumnos que tuvieran únicamente una evaluación pendiente, solo se presentarán a dicha evaluación. Aquellos con dos o más realizarán el examen global.

---

### Funcionamiento de Curso


* Convocatoria extraordinaria: A final de Junio, aquellos alumnos que no hubieran superado el curso podrán hacerlo mediante el examen correspondiente a dicha convocatoria.

---
## Cuadernillos

+ ¿8 euros justos?
