---
title: Presentación 2º Bachillerato
summary: 
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2019-09-10T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: serif
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: dracula
---
{{< slide background-image="featured.jpg" >}}


# Acogida 2º Bachillerato 
## CIT

[Curso 19-20](https://crdguez.gitlab.io/)

---

## Presentaciones

---

## Equipo docente

---

{{< gdocs src="https://docs.google.com/document/d/186x7_xvZ7utES86cmusYR0tgijAbgn5HARqfn0V3c9k/edit?usp=sharing" >}}

---

## Horario

---

{{< gdocs src="https://docs.google.com/document/d/1FKjcq7S4E9FSmzuEH3xzf_kHZfaoQmLMZcObJ-MGbLI/edit?usp=sharing" >}}

---

## EVAU

[web informativa de UNIZAR](https://academico.unizar.es/acceso-admision-grado/evau/nueva)

---

{{< figure library="true" src="esquemaevau.png"  lightbox="true" >}}

---

### Algunas consideraciones en la EVAU

- Ceñirse al tiempo, no se pregunta
- Bolígrafo o pluma negra o azul
- No se puede usar Tipex
- No se pueden usar calculadoras programables

---

### Consejos para el curso que empieza

- No abusar del "pasillo", temarios ajustados
- Planificar el estudio, llevar las asignaturas al día
- Evitar memorizar -- aprendizajes significativos

---

### Cuadernillo de matemáticas

- 8€, próximo lunes

---

# ¿Preguntas?

