---
title: Presentación 4ºESO
summary: 
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2020-09-08T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: serif
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: dracula
---
{{< slide background-image="featured.jpg" >}}


# Acogida 4º ESO B

[Curso 20-21](https://crdguez.gitlab.io/)

---

## Presentaciones

* Carlos Rodríguez  (Matemáticas) --> rodriguezjasoc@iespedrocerrada.org , 
* {{% staticref "files/matricula_optativas.pdf" "newtab" %}}Matrícula y optativas{{% /staticref %}}
* [Correos electrónicos de alumnos](https://docs.google.com/spreadsheets/d/1-xLEHPGCgj-moBbJJdnLApG_Gfjf4MpHSdeL6YiUmPA/edit#gid=0)

---

## Equipo docente y horario

* {{% staticref "files/horario_grupo_profesores_bruto.pdf" "newtab" %}}Horario y equipo (bruto) {{% /staticref %}}
* [Horario y equipo docente en limpio](https://docs.google.com/document/d/12SJxccBTMvnmIjdhZ4aoy3gnIEzSNKXWSsHLb3UsZes/edit?usp=sharing)

---

## COVID

{{< youtube iojY4d0JyTE>}}

---

### Plan de Contingencia Covid

* [Información relevante para alumnos](https://drive.google.com/file/d/1gK_rgd47vOeE7ys5yaCco0NUlRdW5FOT/view)
* [Plan de contingencia subrayado](https://drive.google.com/file/d/16di9MXASEUjIKS5qlDO9oVGxmN0vF-y7/view?usp=sharing)

---

# ¿Preguntas?

