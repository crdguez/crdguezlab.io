__EVAU__

__EVALUACIÓN PARA EL ACCESO A LA UNIVERSIDAD__

Se divide en dos fases

__Fase Obligatoria__

__Fase Obligatoria__

Estafase es obligatoria para los estudiantes de Bachillerato LOMCE que deseen acceder a estudios oficiales de grado y <span style="color:#FF0000"> _tiene validez indefinida_ </span> \. Está constituida por 4 ejercicios sobre las siguientes materias:

__Fase Obligatoria__

LENGUACASTELLANA Y LITERATURA II

HISTORIADE ESPAÑA

LENGUAEXTRANJERA II: INGLÉS\, FRANCÉS O ALEMÁN

Unaasignatura troncal general de entre las que marcan modalidad en el Bachillerato:

_Ciencias_

_MATEMÁTICAS_  _II_

<span style="color:#0033CC"> _Humanidades_ </span>

<span style="color:#0033CC"> __LATÍN__ </span>  <span style="color:#0033CC"> __II__ </span>

<span style="color:#006600"> _Ciencias Sociales_ </span>

<span style="color:#006600"> __MATEMÁTICAS APLIC A__ </span>  <span style="color:#006600"> __LAS__ </span>  <span style="color:#006600"> __C\.S\.__ </span>  <span style="color:#006600"> __II__ </span>

__Fase Obligatoria__

Para los estudiantes de Bachillerato LOMCE es requisito haber cursado las materias de las que se examinen en esta fase\.

Lacalificación de la fase obligatoria será la media aritmética de las calificaciones de los cuatro ejercicios\. Esta calificación deberá ser igual o superior a 4 puntos para que pueda ser tenida en cuenta para el cálculo de la _nota de acceso_  _\._

__Fase Obligatoria__

__Nota de acceso__

Esta nota determina el derecho del estudiante a acceder al sistema universitario y se calcula del siguiente modo:

<span style="color:#FF0000"> __NOTA ACCESO =__ </span>

<span style="color:#FF0000"> __60% de la calificación final de Bachillerato__ </span>

<span style="color:#FF0000"> __\+__ </span>

<span style="color:#FF0000"> __40% de la calificación de la fase obligatoria__ </span>

El estudiante tendrá acceso a la universidad cuando su nota de acceso sea __igual o superior a cinco puntos\.__

Sufinalidad es la de mejorar la nota de admisión\, pudiendo llegar como máximo hasta los 14 puntos\.

En esta fase el estudiante se podrá examinar de hasta cuatro materias :

Dematerias troncales de opción de 2º de Bachillerato\.

Dealguna de las materias troncales generales determinadas por la modalidad en el Bachillerato\, siempre que no haya sido incluida en la Fase obligatoria

No se exige que el estudiante haya cursado en el

Bachilleratolas materias que elija para examinarse en esta fase\.

__Fase Voluntaria__

__Nota de admisión__

Permitemejorar la nota de acceso mediante la superación de determinadas materias de laEvAUque tengan un parámetro de ponderación asociado al estudio de grado solicitado\.

__Fase Voluntaria__

__Nota de admisión__

_NOTA DE ADMISIÓN =_

_Nota acceso \+ a\*M_  _1_  _\+ b\*M_  _2_

__M1\, M2 =__ las calificaciones de un máximo de dos materias __superadas__ en laEvAUque proporcionen mejor nota de admisión para el gradosolicitado

a\, b = parámetros de ponderación de materias de laEvAU

__Fase Voluntaria__

__Nota de admisión__

Una materia se considera aprobada con una nota mínima de 5 puntos\. Las materias suspendidas no se tienen en cuenta para el cálculo de la nota de admisión\.

Las calificaciones de las materias M1 y M2  solo podrán ser aplicadas únicamente a los procedimientos de admisión a estudios de grado que se convoquen para acceder en los dos cursos académicos siguientes al de superación de las materias\.

