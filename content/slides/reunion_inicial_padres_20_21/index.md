---
title: Reunión con padres
summary: Jornada de
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2020-10-19T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: beige
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: github
  
  
---

{{< slide background-image="featured.jpg" >}}

# Reunión con padres 

[Curso 20-21](https://crdguez.gitlab.io/slides/reunion_inicial_padres_20_21/#/)

---
### Mientras esperamos ...

| [Esta presentación](https://crdguez.gitlab.io/slides/reunion_inicial_padres_20_21/#/) | [Horario y equipo](https://crdguez.gitlab.io/files/horario_grupo_profesores_bruto.pdf) |
| :-----------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------: |
| {{< figure src="qr_presentacion.png" title="" width="80%" height="80%">}} | {{< figure src="qr_horario.png" title="" width="80%" height="80%">}} |

---

### Introducción

- Carlos Rodríguez Jaso (Matemáticas) - *rodriguezjasoc@gmail.com*
- Justificación de la reunión
- Agradecer su presencia

---

### Características del grupo
---

- El grupo viene determinado por la elección de **itinerario** (**FyQ y Economía o Biología**):
  * 21 alumnos **bilingües**
  * 7 alumnos **no bilingües**
- No hay ningún criterio pedagógico

---

### Características del curso académico
---

* Curso terminal de etapa --> **Título de la ESO**

* Todavía no hay aprobada ninguna normativa respecto a **la titulación**. La Lomloe y el Covid determinará la normativa

* **COVID**
---

#### Modelo de semipresencialidad 

{{< figure src="semipresencialidad.png" title="" width="50%" height="50%">}}


* Más información: [https://educa.aragon.es/documents/20126/978568/ANEXO+III++Secundaria.pdf/f10c177a-3712-7313-f70b-de6cd3556d34?t=1598610769484](https://educa.aragon.es/documents/20126/978568/ANEXO+III++Secundaria.pdf/f10c177a-3712-7313-f70b-de6cd3556d34?t=1598610769484)
---

* Número máximo en los turnos --> **15 personas**
* Organización personal del trabajo:
	- Correo electrónico
	- **Google Classroom**
* Corresponsabilidad para que el modelo funcione

---

#### SEGURIDAD EN EL AULA

---

- 1 sólo recreo
- Actividades extraescolares: 
  - No habrá salidas y excursiones lo menos durante el primer trimestre. 
- Sí que habrá alguna actividad complementaria como la actividad de **piscina**. 

--- 

 - **Plan de contingencia**:
      - **Limpieza** de manos: En el acceso del centro; Al inicio de toda sesión.
      - **Ventanas**
      - **Puertas abiertas** para evitar el contacto de pomos 
      - Mesas no intercambiables
      - No se usan las perchas 
       -Baños: Cada planta tiene sus baños/planta baja

--- 

- Si se detecta los síntomas en el centro: 
    * El docente Informa al Equipo Directivo y el Equipo COVID del centro activa el protocolo: 
      * Aislamiento en cuarto COVID hasta que lo recoge la familia si es menor 
      * Llama al centro médico 
      * Salud determina el grado de confinamiento del aula

- Si se detecta síntomas en el domicilio:
    * Protocolo: declaración responsable de las familias. 

--- 

- **Reducción de tránsito** en el centro. 
    * Durante el curso 2020-2021 **no** se permitirá el acceso de familiares al IES **sin cita previa**.
    *	Cualquier trámite o consulta a realizar con la **secretaría** del centro se realizará a través de: *iesutebo@educa.aragon.es* 
    *	Comunicación con docentes y/o tutores se realizará a través del **correo electrónico** corporativo de cada docente.
    *	Comunicación con el equipo directivo se realizará mediante el **correo electrónico** corporativo tanto de la dirección del centro como de jefatura de estudios. 


---

#### Horario y Equipo docente

[Hoja informativa](https://docs.google.com/document/d/12SJxccBTMvnmIjdhZ4aoy3gnIEzSNKXWSsHLb3UsZes/edit?usp=sharing)

---

#### Calendario

- Festivos locales: 5 marzo y 18 junio
- Evaluaciones: 10-12, 17-03 y 16-06

---

#### Tutorías

 {{< figure src="instruccion_tutoria.png" title="" width="50%" height="50%">}}

---

#### Procedemiento tutorías

* Cita previa con una semana de antelación:
  - Alumno
  - Correo electrónico *rodriguezjasoc@gmail.com*
* Martes 10:15 a 11:10
* De manera telemática: Correo electrónico o videoconferencia
* Excepcionalmente de manera presencial

---

### Otras cuestiones relativas al funcionamiento del centro

--- 

#### Gestión de incidencias:

* SIGAD: faltas, retrasos y calificaciones evaluación.
Faltas de asistencia y formas de justificarlas: referencia al justificante y a SIGAD.
Exenciones en educación física deben aportar justificante médico

---

#### Comunicados del centro 

* TokApp School: APLICACIÓN para comunicaciones del IES a familias

---

- Dudas, cuestiones, preguntas ...
- ¡Gracias por vuestra asistencia!

---
