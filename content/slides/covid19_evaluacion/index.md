---
title: COVID - Evaluación 
summary: 
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2020-05-02T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: serif
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: dracula
---

{{< slide background-image="featured.jpg" >}}

## Tercer trimestre

2º Bachillerato - Curso escolar 2019 / 2020

[Directrices de actuación](https://educa.aragon.es/documents/20126/680999/20200429-ORDEN+ECD+357+2020%2C+de+29+de+abril.pdf/f50c84a2-6b20-961e-f7a1-81e9772390c4?t=1588150545481)

---

## Índice

- DESARROLLO DEL TERCER TRIMESTRE CURSO ESCOLAR 2019/2020 Y FLEXIBILIZACIÓN DE
LOS PROCESOS DE EVALUACIÓN, PROMOCIÓN Y TITULACIÓN
		1.- ORGANIZACIÓN DEL TERCER TRIMESTRE.
		1.1.- Calendario de fin de curso 2019/2020.
1.2.- Programaciones y contenidos para trabajar en el tercer trimestre.
1.3.- Orientación y tutoría.
1.4.- Trabajo colaborativo para la coordinación de las diferentes materias.
2.- EVALUACIÓN. CONSIDERACIONES GENERALES.
2.1.- Consideraciones generales sobre proceso de evaluación.
2.2.- Referentes de la evaluación.
2.3.- Instrumentos de evaluación
2.4.- Modificación de las condiciones de evaluación.
2.5.- Criterios de evaluación y calificación.
3.- EVALUACIÓN DE 1o DE BACHILLERATO Y PROMOCION.
3.1.- Criterios de promoción.
3.2.- Evaluación final extraordinaria.
4.- EVALUACIÓN DE 2o DE BACHILLERATO Y OBTENCIÓN DEL TÍTULO.
4.1.- Criterios de para la obtención del Título de Bachillerato.
4.2.- Evaluación final extraordinaria de 2o de Bachillerato.
4.3.- Documentos de evaluación y cálculo de nota media del título.
---

## Fase Obligatoria

- LENGUA CASTELLANA Y LITERATURA II

- HISTORIA DE ESPAÑA

- LENGUA EXTRANJERA II: INGLÉS,  FRANCÉS O ALEMÁN

Una asignatura troncal general de entre las que marcan modalidad en el Bachillerato:

- MATEMÁTICAS II, MATEMÁTICAS APLICADAS O LATÍN

---

## Fase Obligatoria

- Para los estudiantes de Bachillerato LOMCE es requisito haber cursado las materias de las que se examinen en esta fase.

- La calificación de la fase obligatoria será la media aritmética de las calificaciones de los cuatro ejercicios. 

- Esta calificación deberá ser igual o superior a 4 puntos para que pueda ser tenida en cuenta para el cálculo de la _nota de acceso_ .

---

## Nota de acceso

Esta nota determina el derecho del estudiante a acceder al sistema universitario y se calcula:

<span style="color:#FF0000"> __NOTA ACCESO =__ </span>

<span style="color:#FF0000"> __60% de la calificación final de Bachillerato__ </span>

<span style="color:#FF0000"> __\+__ </span>

<span style="color:#FF0000"> __40% de la calificación de la fase obligatoria__ </span>

El estudiante tendrá acceso a la universidad cuando su nota de acceso sea __igual o superior a cinco puntos\.__

---

## Fase voluntaria

Su finalidad es la de mejorar la nota de admisión, pudiendo llegar como máximo hasta los 14 puntos.

---

## Fase voluntaria

Se podrá examinar de hasta cuatro materias :

- De materias troncales de opción de 2º de Bachillerato
- De alguna de las materias troncales generales determinadas por la modalidad en el Bachillerato, siempre que no haya sido incluida en la Fase obligatoria

No se exige que el estudiante haya cursado en el Bachillerato las materias que elija.

---

## Nota de admisión

Permite mejorar la nota de acceso mediante la superación de determinadas materias de la EvAU que tengan un parámetro de ponderación asociado al estudio de grado solicitado.

---

## Nota de admisión

<span style="color:#FF0000">_NOTA DE ADMISIÓN =_</span>

<span style="color:#FF0000">_Nota acceso \+ a\*M_  _1_  _\+ b\*M_  _2_</span>

__M1, M2 =__ las calificaciones de un máximo de dos materias __superadas__ en laEvAUque proporcionen mejor nota de admisión para el gradosolicitado

__a, b__ = parámetros de [ponderación](https://academico.unizar.es/sites/academico.unizar.es/files/archivos/acceso/norma/ponder19205.pdf) de materias de la EvAU

---

## Nota de admisión

Una materia se considera aprobada con una nota mínima de 5 puntos\. Las materias suspendidas no se tienen en cuenta para el cálculo de la nota de admisión\.

- Las calificaciones de las materias M1 y M2  solo podrán ser aplicadas únicamente a los procedimientos de admisión a estudios de grado que se convoquen para acceder en los dos cursos académicos siguientes al de superación de las materias\.

---

{{< figure src="resumen2.jpg" title="Resumen" numbered="true" height="600" lightbox="true" >}}

---
## Para acabar ...

-  [Notas de corte](https://academico.unizar.es/acceso-admision-grado/corte)
-  **Preguntas, dudas, ...**