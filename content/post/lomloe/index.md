---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "LOMLOE"
subtitle: "Publicada **la LOMLOE**, por la que se modifica la ley de educación"
summary: ""
authors: []
tags: [Lomloe]
categories: []
date: 2021-01-24T09:53:42+01:00
lastmod: 2021-01-24T09:53:42+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Pues nada, ya tenemos una nueva modificación de la ley de educación: La **LOMLOE**, o la llamada coloquialmente **ley Celaá**.

La implantación de la misma se hará, si no hay derogaciones, de manera progresiva:

- Curso 2021-2022: Primero y tercero de la ESO.
- Curso 2022-2023: Segundo y cuarto de la ESO

Con dicha reforma la ley de Educación queda así: [https://www.educacionyfp.gob.es/dam/jcr:45db760b-02bd-4b57-8732-e3abae6bffb3/loe-con-lomloe-2021-01-18.pdf](https://www.educacionyfp.gob.es/dam/jcr:45db760b-02bd-4b57-8732-e3abae6bffb3/loe-con-lomloe-2021-01-18.pdf)

