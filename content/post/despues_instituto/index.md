---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "¿Después de acabar la ESO qué?"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2021-03-19T06:33:39+01:00
lastmod: 2021-03-19T06:33:39+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---


Ciclos formativos:

* Información del proceso de admisión y **notas de corte** para el **curso 19-20**: [enlace](https://www.centroseducativosaragon.es/Public/noticias_detalle.aspx?id=171)
* Notas de corte del **curso 17-18**: [enlace](http://www.iessantiagohernandez.com/notas-de-corte-del-curso-201718/)
* [**Información sobre el proceso de admisión 21-22**](https://educa.aragon.es/-/fp/noticias/admision-curso-21-22?redirect=%2Fweb%2Fguest%2Fnoticias)


Grados superiores en la Universidad de Zaragoza:

* [Notas de corte](https://academico.unizar.es/acceso-admision-grado/corte) 
* [Ponderaciones](https://academico.unizar.es/sites/academico.unizar.es/files/archivos/acceso/norma/ponder2122.pdf)

[Itinerarios de Bachillerato en el IES Pedro Cerrada](https://drive.google.com/file/d/1T9-ULRYDBCajSqxwCbOWl1sVyjqQua9q/view)



...






