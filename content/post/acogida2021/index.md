---
title: 'Acogida 20-21'
subtitle: 'Inicio de curso 2020-2021'
summary: "Inicio de curso 2020-2021"
authors:
- admin
tags:
- Inicio
- tutoria2021
categories:
- inicio
date: "2020-09-08T00:00:00Z"
featured: false
draft: false


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/)'
  focal_point: ""
  preview_only: false

slides: "4eso_acogida_alumnos"

---

Este año me toca ser tutor de 4ºESO.

[Enlace](https://crdguez.gitlab.io/slides/4eso_acogida_alumnos/#/) a la presentación del día de acogida.

Desde aquí deseo lo mejor a mi alumnado en este curso tan relevante. Estoy convencido de que con el esfuerzo de todos conseguiremos alcanzar los objetivos que nos propongamos. ¡Ánimo y a por ello!