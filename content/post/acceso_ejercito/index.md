---
title: 'Acceso al Ejército'
subtitle: '¿Cómo acceder al ejército?'
summary: "Información sobre los requisitos para acceder al cuerpo de oficiales del ejército"
authors:
- admin
tags:
- ejército
categories:
- inicio
date: "2020-02-27T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: ''
  focal_point: ""
  preview_only: false

# slides: "presentacion_evau"
---

[folleto informativo de acceso a estudios en centros docentes militares](www.reclutamiento.defensa.gob.es/pdf/folletos-defensa/ofi-sin/OFICIALES-CG.pdf)
