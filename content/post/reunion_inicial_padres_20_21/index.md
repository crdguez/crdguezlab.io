
---
title: "Reunión inicial con padres. Curso 2020-2021"
authors:
- admin
date: "2020-10-19T00:00:00Z"
doi: ""

draft: false

# Schedule page publish date (NOT publication's date).
#publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: 
# Summary. An optional shortened abstract.
summary: Convocatoria, enlace a la presentación e información relevante

tags:
- tutoria2021
featured: true

#url_pdf: "prueba.pdf"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:


# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: "reunion_inicial_padres_20_21"
#url_slides: "https://crdguez.github.io/mis_presentaciones/cledu.html#/title-slide"
---

{{% alert note %}}
Puedes acceder a la presentación pulsando en *Diapositivas* arriba.
{{% /alert %}}

El próximo míercoles 21 de octubre a las 18:00 hay convocada una reunión telemática con las familias de los alumnos de 4º de la ESO B.

Os dejo información correspondiente al grupo y horario:

{{< gdocs src="https://docs.google.com/document/d/12SJxccBTMvnmIjdhZ4aoy3gnIEzSNKXWSsHLb3UsZes/edit?usp=sharing" >}}
