---
title: Información Docente
summary: Hoja de Inicio de Curso, Programaciones, etc.
tags:
- 1eso
date: "2020-09-12T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://github.com/crdguez/mat4ac/tree/master/ejercicios/build

weight: 10

image:
  caption: Photo  on [Unsplash](https://unsplash.com/)
  focal_point: Smart
---

<!-- * {{% staticref "files/inicio_curso_4_academicas.pdf" "newtab" %}}Hoja de Inicio de Curso{{% /staticref %}} -->
* [Hoja de inicio de curso - 1ºESO](https://docs.google.com/document/d/1o-QVTSgIyixXnNgEdcei3Y8qbsYKFC1b/edit?usp=sharing&ouid=106926421917771551298&rtpof=true&sd=true)
* [Normativa de ESO y Bachillerato](https://educa.aragon.es/web/guest/-/normativa-eso-bachillerato):
	- {{% staticref "files/curriculo_mat_1_2_eso.pdf" "newtab" %}}Currículo de matemáticas de 1º y 2º ESO{{% /staticref %}}
<!-- * [COVID](https://educa.aragon.es/covid-19) -->
* Libro de texto: Matemáticas 1. ESO. Anaya. ISBN: 978-84-698-1792-6. Más información en el [classroom](https://classroom.google.com/c/MzIwMzQ2NzI0NzE2/m/MzIwMzQ2NzI0Nzk4/details) de clase.
