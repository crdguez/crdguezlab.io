---
title: Carpeta Drive Compartida
summary: Carpeta con recursos compartida
tags:
- 1ESO

date: "2099-09-01T00:00:00Z"

weight: 30

# Optional external URL for project (replaces project detail page).
external_link: https://drive.google.com/drive/folders/1ny5e3wydBQSYoGsLLwbKHOjvMjQV7kQ5


image:
  caption: Unsplash
  focal_point: Smart
---
