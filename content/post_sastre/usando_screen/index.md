---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Usando el comando **screen**"
subtitle: ""
summary: "El comando screen para manejar varias terminales a la vez"
authors: []
tags: []
categories: []
date: 2020-12-26T08:04:37+01:00
lastmod: 2020-12-26T08:04:37+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**Referencia:**[https://www.howtogeek.com/662422/how-to-use-linuxs-screen-command/](https://www.howtogeek.com/662422/how-to-use-linuxs-screen-command/)

El caso es que tengo montada una raspberry pi a la que accedo por *ssh*. Así que en ocasiones necesito tener varias terminales.

Pues bien, mediante el comando *screen* puedo tener las terminales que quiera así como presentarlas.

Normalmente sigo estos pasos:

```
screen -S monitor
```

Creo una "pantalla" llamada *monitor*. En ese momento está *atachada* si queremos *desatacharla*, es decir dejarla en segundo plano, tenemos que pulsar *Ctrl+A+d*.

En ese momento nos aparecera algo parecido a esto:

```
[detached from 1377.monitor]
```
Y volveremos a la terminal desde la que hemos lanzado *screen*. Si queremos volver al terminal *screen* tenemos que escribir:

```
screen -r monitor
```
o

```
screen -r 1377
```

Veamos ahora como podemos dividir la *pantalla* y así poder controlar o monitorizar dos cosas a la vez:

- Pulsamos *Ctrl+A+|* y se nos separará la pantalla
- Pulsamos *Ctrl+A+C* y creará una nueva ventana, será la #1, mientras que la anterior será la #0.
- Pulsando *Ctrl+A+Tab* nos moveremos entre ventanas
- En la ventana correspondiente, selecciono la terminal que quiero que aparezca con *Ctrl+A+0* o  *Ctrl+A+1*

Espero que os haya resultado útil.





