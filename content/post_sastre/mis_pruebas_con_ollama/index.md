---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Mis pruebas con ollama"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2024-11-16T16:12:34+01:00
lastmod: 2024-11-16T16:12:34+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

He empezado a hacer pruebas con **Ollama**. Para ello he creado:

* Docker Ollama: 
	- `docker start ollama` . Si queremos ejecutar algo en el *shell* de este contenedor: `docker exec -it ollama sh`
	- Podemos probar que el servidor está levantado con [http://localhost:11434](http://localhost:11434) o en el terminal escribir: `curl http://localhost:11434`

* Docker OpenWebUI
	- `docker start open-webui` y luego hay que ir al navegador a [http://localhost:8080](http://localhost:8080) 
