---
title: 'Realizando pantallazos'
subtitle: 'Cómo capturar pantalla desde Lubuntu'
date: "2019-08-09"
featured: true
---
Para poder capturar pantalla desde Lubuntu tenemos que ejecutar:

```
scrot -s 'path/filename.png'
```