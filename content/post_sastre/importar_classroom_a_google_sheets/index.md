---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Importar classroom a google sheets"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2022-05-26T19:43:31+02:00
lastmod: 2022-05-26T19:43:31+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Necesitaba exportar a mis alumnos de Classroom a una hoja de cálculo. He usado Google Sheets.

La información sobre cómo hacerlo la he encontrado [aquí](https://spreadsheetpoint.com/google-sheets-import-json-guide/). Básicamente he seguido los siguientes pasos:

* Con Google Takeouts he exportado los datos de las clases en formato JSON.
* En Google Sheets he creado una nueva función de exportación de Json utilizando Apps Script. He sacado el código de [aquí](https://gist.github.com/paulgambill/cacd19da95a1421d3164?ref=hackernoon.com):
* Por último importo los datos desde una celda con la función anterior.

