---
eye_catch: null
mathjax: true
tags:
  - LaTeX
title: "Sí, otro cuaderno de profesor"
subtitle: "YATP. Yes, another teacher planner"
date: "2019-08-28T00:00:00Z"
markup: mmark
---

Comparto el cuaderno de profesor que voy a utilizar este curso: [https://gitlab.com/crdguez/cuaderno_profesor](https://gitlab.com/crdguez/cuaderno_profesor).  En concreto: [cuaderno.pdf](https://gitlab.com/crdguez/cuaderno_profesor/raw/master/cuaderno.pdf?inline=false).


Siéntete libre de usarlo como consideres, ya que en el enlace anterior tienes acceso a todo el código para reutilizarlo y/o mejorarlo.


## Instrucciones

Necesitarás tener instalado $$\LaTeX$$ en tu ordenador  para poder compilar las modificaciones que quieras hacer. Más información en [https://www.latex-project.org/](https://www.latex-project.org/).

El documento principal es *cuaderno.tex* y el siguiente árbol representa sus dependencias:

```
. cuaderno.tex
+-- conf.tex   
+-- preambulo_calen.tex   
+-- preambulo_hojas.tex  
+-- calen.tex
|   +-- formato_calen.tex
+-- calen_land.tex
|   +-- formato_calen.tex
+-- notas.tex
+-- page.tex
```



Mirando el código es fácil ver qué contiene y qué hay que modificar para adaptarlo a tu gusto:

- En *cuaderno.tex* se establece en **qué orden y cuántas páginas contiente el cuaderno**.

-  El archivo *conf.tex* permite configurar algunos parámetros del documento. Modifícalo para que se adapte a tus necesidades:

En mi caso tengo:

```tex
\def\titulo{Cuaderno de Seguimiento}
\pgfmathsetmacro{\year}{2019}
\pgfmathsetmacro{\nyear}{int(\year + 1)}
\def\autor{Carlos Rodríguez Jaso}
\def\departamento{Matemáticas}
\def\instituto{IES Pedro Cerrada (Utebo)}

```

- El archivo *calen.tex* contiene el calendario escolar en formato *tikz* y se ha diseñado para poder anotar brevemente qué se hace en cada sesión.
- El archivo *calen_land.tex* muestra el calendario en apaisado y reducido a dos páginas. Se ha diseñado para marcar las fechas clave.
- En *formato_calen.tex* se indica qué fechas son festivas en los calendarios. Por ejemplo:

```tex
\ifdate{workday}
{
  % normal days are white
  \tikzset{every day/.style={fill=white}}
  % Vacation  gray background
  % antes de inicio de curso
  \ifdate{between=\year-09-01 and \year-09-12}{%
    \tikzset{every day/.style={fill=gray!30}}}{}
  % vacaciones de navidad
  \ifdate{between=\year-12-21 and \nyear-01-06}{%
    \tikzset{every day/.style={fill=gray!30}}}{}
  % vacaciones de semana santa
  \ifdate{between=\nyear-04-06 and \nyear-04-13}{%
    \tikzset{every day/.style={fill=gray!30}}}{}
  % fin de curso
  \ifdate{between=\nyear-06-23 and \nyear-08-32}{%
    \tikzset{every day/.style={fill=gray!30}}}{}
  % Puentes y festivos del curso
  \ifdate{equals=10-10}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=10-11}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=11-01}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=12-06}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=12-09}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=03-05}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=03-06}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=04-23}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=04-24}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=05-01}{\tikzset{every day/.style={fill=gray!30}}}{}
  \ifdate{equals=06-19}{\tikzset{every day/.style={fill=gray!30}}}{}

}{}
% Saturdays and half holidays (Christma's and New year's eve)
\ifdate{Saturday}{\tikzset{every day/.style={fill=red!10}}}{}
% Sundays and full holidays
\ifdate{Sunday}{\tikzset{every day/.style={fill=red!20}}}{}

```

- El archivo *nota.tex* muestra una tabla para incluir a los alumnos y notas que se vayan registrando día a día

- El archivo *page.tex* presenta una hoja cuadriculada. El tipo de cuadrícula se especifica con la variable *\def\usepat{}* y que puede ser:

  - std: cuadrícula pequeña

  - stdeight: cuadrícula más grande

  - majmin: Cuadrícula con borde de 4x4 cuadrados

Espero que sea útil

## Atribuciones

Este cuaderno del profesor  está creado a partir de los siguientes recursos:

- [LaTeX-Graph-Paper de mcnees](https://github.com/mcnees/LaTeX-Graph-Paper)
- [Calendario de Rolf Niepraschk](http://www.texample.net/tikz/examples/a-calender-for-doublesided-din-a4/)
- [A complete graph de  Jean-Noël Quintin](http://www.texample.net/tikz/examples/complete-graph/)

Gracias por compartir
