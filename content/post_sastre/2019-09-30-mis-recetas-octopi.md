---
eye_catch: null
mathjax: true
tags:
  - 3D
  - Impresoras 3D
  - Octopi,Octoprint
title: "Mis recetas Octopi"
subtitle: "Lo que voy haciendo para montar octopi en mi impresora 3D"
date: "2019-09-30T00:00:00Z"
markup: mmark
---
La información que he utilizado hasta el momento es:

- [Octopi](https://octoprint.org/download/). Para el [perfil](https://howchoo.com/g/ntg5yzg1odk/using-octoprint-with-the-creality-ender-3-3d-printer) de la Ender
- [TouchUi](https://github.com/BillyBlaze/OctoPrint-TouchUI) Ojo, que yo tengo la tft 3.2
    * [Cuando tuve problemas con xserver-xorg-input-evdev](https://www.gitmemory.com/issue/goodtft/LCD-show/152/509587437) 	
    * Pantalla negra: [https://github.com/BillyBlaze/OctoPrint-TouchUI/wiki/Setup:-Troubleshooting#boots-to-blackscreen](https://github.com/BillyBlaze/OctoPrint-TouchUI/wiki/Setup:-Troubleshooting#boots-to-blackscreen)
- [Octotouch tiene una buena lista de instrucciones para instalar TouchUi desde Octopi](https://github.com/FlyingT/OctoTouch/wiki/Complete-Setup-Guide)
- Para añadir octoprint en CURA: Preferences/printer/Ender3/Connect Octoprint. Antes he tenido que instalar el plugin en Marketplace