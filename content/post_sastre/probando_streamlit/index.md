---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Probando streamlit"
subtitle: "Probando la librería streamlit de Python para generar un dashboard"
summary: ""
authors: []
tags: []
categories: []
date: 2021-01-27T07:13:34+01:00
lastmod: 2021-01-27T07:13:34+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---


(En construcción, y ya veremos si lo acabo ...)

Estoy probando [Streamlit](https://www.streamlit.io/).

## Lista de pasos

* Crear un contenedor docker para trabajar en él. Sigo esto: [https://hub.docker.com/r/tomerlevi/streamlit-docker](https://hub.docker.com/r/tomerlevi/streamlit-docker)
* He modificado la imagen para añadir *sympy* y tener las versiones más recientes de los paquetes
* He creado un fichero *main,py* con el código de *streamlit*. Si no tengo el docker creado, lo creo con el siguiente comando:

```
docker run -it -p 8501:8501 -v $PWD:/app crdguez/streamlit main.py
```

### Intentando acceder a datos privados

* Puedes acceder a un fichero "raw" cambiando la dirección correspondiente: https://gitlab.com/crdguez/actas_evaluacion/-/raw/master/importado1.csv en lugar de https://gitlab.com/crdguez/actas_evaluacion/-/blob/master/importado1.csv
* Y a ese fichero lo puedes llamar desde python con la librería **requests**: [https://stackoverflow.com/questions/64585328/why-can-rs-read-csv-read-a-csv-from-gitlab-url-when-pandas-read-csv-cant](https://stackoverflow.com/questions/64585328/why-can-rs-read-csv-read-a-csv-from-gitlab-url-when-pandas-read-csv-cant)
* Como los ficheros anteriores son privados, puedes acceder desde python a ellos a través de la API de gitlab: [https://python-gitlab.readthedocs.io/en/stable/](https://python-gitlab.readthedocs.io/en/stable/)
* Desde Streamlit puedes usar **secrets**: [https://www.notion.so/Secrets-Management-730c82af2fc048d383d668c4049fb9bf](https://www.notion.so/Secrets-Management-730c82af2fc048d383d668c4049fb9bf)

Aquí van mis pruebas de intentar obtener ficheros desde gitlab que posteriormente los llevaré a stremalit:

```
curl --request GET 'https://gitlab.com/api/v4/projects/16754108/repository/files/titulacion2021.md/raw'
```

* La información la he sacado de [https://dario-djuric.medium.com/retrieving-raw-files-via-gitlab-api-2b86ac1183b0](https://dario-djuric.medium.com/retrieving-raw-files-via-gitlab-api-2b86ac1183b0)
* Para importar el fichero al dataframe: [https://stackoverflow.com/questions/64585328/why-can-rs-read-csv-read-a-csv-from-gitlab-url-when-pandas-read-csv-cant](https://stackoverflow.com/questions/64585328/why-can-rs-read-csv-read-a-csv-from-gitlab-url-when-pandas-read-csv-cant)

### Haciendo más pruebas

*  Quiero sacar un listado de carpetas:

```
curl "https://gitlab.com/api/v4/projects/8982377/repository/tree/?ref=master&private_token=mitoken"
```
* Podemos usar *python-gitlab* para consultar carpetas:
```
import gitlab
# Accedemos a gitlab con el token
gl = gitlab.Gitlab('https://gitlab.com', private_token='mitoken')
# seleccionamos el proyecto a partir de id que salga en gitlab
p=gl.projects.get(8982377)
# listamos el directorio raíz
p.repository_tree()
# o el directorio antiguo que está en raíz
p.repository_tree('antiguo')
# o contamos el número de archivos que tiene
len(p.repository_tree('antiguo'))
```

* Para acceder a ficheros que están dentro de carpetas hay que separarlas con %2F: https://gitlab.com/api/v4/projects/8982377/repository/files/datos_actas%2F20_21%2F4B%2Fimportado1.csv/raw?ref=master&private_token=mitoken
* 

