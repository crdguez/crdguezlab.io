---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Automaquetando vídeos con Python"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-09-02T09:33:45+02:00
lastmod: 2020-09-02T09:33:45+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false


links:
#  - icon_pack: fab
 #   icon: twitter
 #   name: Follow
  #  url: 'https://twitter.com/Twitter'
 # - icon_pack: fab
   # icon: gitlab
   # name: Originally published on Medium
    # url: 'https://medium.com'

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---


Teniendo en cuenta que desgraciadamente por las circunstancias actuales voy a tener que preparar vídeos con explicaciones y luego se pierde bastante tiempo subiéndolos a YouTube se me ocurrió que sería una buena idea intentar automatizar todo lo que pueda el proceso. 

Además, ya puestos, como durante el curso pasado los vídeos que hice quedaron "bastante cutres", al menos ahora pretendo que tengan una portada con su título. Huelga decir que no pretendo hacer vídeos "superprofesionales", básicamente porque no soy youtuber. Me conformo con poder colgar material que pueda ser útil al alumnado en las circunstancias que nos encontramos.

El proceso que voy a explicar se divide en dos partes por tanto:

* Edición del vídeo con [MoviePy](https://zulko.github.io/moviepy/) para, en mi caso, añadir una portada . Si echas un vistazo a [MoviePy](https://zulko.github.io/moviepy/) verás que puedes hacer bastantes más cosas, pero por el momento me sirve lo anterior.  
* Subir el vídeo "editado" a mi cuenta de "Youtube"

## Añadir una portada con [MoviePy](https://zulko.github.io/moviepy/) a un vídeo que hayamos hecho

[MoviePy](https://zulko.github.io/moviepy/) es una librería de Python para editar vídeos. En mi caso voy a partir de una imagen que me servirá de fondo de portada y la insertaré al principio del vídeo con el título que quiera que aparezca.

EL código que lo hace es el siguiente:

{{< gist crdguez 0029d67dfca2fb2259441efad2bcd49c  >}}

Y la portada que he utilizado es:

{{< figure src="portada.png" title="Portada" >}}


## Subir el vídeo "editado" a mi cuenta de "Youtube"

He sequido el código que aparece en la documentación de [google](https://developers.google.com/youtube/v3/code_samples/python#upload_a_video). Sin embargo,  el código tenía errores para Python 3.

El código modificado queda así:

{{< gist crdguez 62929283c8b1f40d442a2c116e0d33be  >}}

Y este es el resultado final:

{{< youtube js_pawaHZuI>}}


