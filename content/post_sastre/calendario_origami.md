---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Calendario de origami - Dodecaedro rómbico"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2022-12-21T06:48:42+01:00
lastmod: 2022-12-21T06:48:42+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Para generarlo, lo podemos hacer desde esta página:

[http://www.toddsplace.ca/rhombic.php](http://www.toddsplace.ca/rhombic.php)
