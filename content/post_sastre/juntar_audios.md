---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Juntar audios de una carpeta"
subtitle: "Script en Python para juntar los audios de una carpeta en un único audio"
summary: ""
authors: []
tags: [python, mp3]
categories: []
date: 2022-03-05T07:34:44+01:00
lastmod: 2022-03-05T07:34:44+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

He modificado el script que encontré en [https://www.thepythoncode.com/article/concatenate-audio-files-in-python](https://www.thepythoncode.com/article/concatenate-audio-files-in-python) 
para que tome todos los audios de una carpeta y los una en único archivo mp3.

Ejemplo de uso

```
$ python concatenate_audio_moviepy.py -d 'carpeta con los archivos mp3' -o output.mp3
```

El código es el siguiente:

{{< gist crdguez 6869767d9504295eb196551a0495776f  >}}
