
---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Bingo con scratch"
subtitle: "Bingo programado con Scratch"
summary: ""
authors: []
tags: []
categories: []
date: 2021-10-07T06:13:34+01:00
lastmod: 2021-10-06T07:13:34+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Tengo intención de llevar algunas actividades de [Ana García Azcarate](https://anagarciaazcarate.wordpress.com/). Para los bingos necesitaría un bingo con bolas para ir sacando números. En lugar de usar un bingo físico he programado uno con Scratch: [Mi bingo con Scratch](https://scratch.mit.edu/projects/580558373/fullscreen/).



