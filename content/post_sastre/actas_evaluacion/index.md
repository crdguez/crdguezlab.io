---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Actas de evaluación"
subtitle: "Repositorio con el código Python que utilizo para generar mis actas de evaluación"
summary: ""
authors: []
tags: []
categories: []
date: 2021-01-12T10:20:43+01:00
lastmod: 2021-01-12T10:20:43+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Comparto el código que utilizo para generar las actas de evaluación a partir de un fichero *csv* con la actilla de notas (tal y como aparece en la plataforma de gestión académica del Gobierno de Aragón - SIGAD)


Tenéis el código y la documentación en el siguiente repositorio de github:

* [https://github.com/crdguez/actas_evaluacion](https://github.com/crdguez/actas_evaluacion)


