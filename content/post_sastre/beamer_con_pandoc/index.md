---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Beamer con pandoc"
subtitle: ""
summary: "Creando presentaciones beamer con pandoc"
authors: []
tags: []
categories: []
date: 2021-01-09T08:40:24+01:00
lastmod: 2021-01-09T08:40:24+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Cuando quiero hacer una presentación de forma rápida utilizo el fantático [pandoc](https://pandoc.org/).

Usando [pandoc](https://pandoc.org/) solo me tengo que preocupar en la escritura de un fichero en *markdown* con el contenido de la presentación.

Veamos un ejemplo:
Creamos un fichero *input.md* con el siguiente contenido:


```
---
title: "¿Qué es Sympy Gamma?"
author: "IES Pedro Cerrada"
institute: "Departamento de Matemáticas"
theme: "Rochester"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: red
linkstyle: bold
aspectratio: 169
titlegraphic: img/portada.png
date:
section-titles: false
toc: false
navigation: "frame"
---

# Sympy Gamma

Enlace a **Sympy Gama**: [https://gamma.sympy.org/](https://gamma.sympy.org/)

![Sympy Gamma](img/pantallazo_sympy_gamma.png){ width=50% }

# ¿Qué es Sympy Gamma?

- Interfaz web para hacer cálculos matemáticos 
- Clon de *Wofram Alpha* ([https://www.wolframalpha.com/](https://www.wolframalpha.com/))

# Diferencias entre **Sympy Gamma** y **Wofram Alpha**

| Sympy Gamma | Wolfram Alpha |
|:------------------------:|---------------------------|
|Utiliza Sympy | Utiliza Mathematica|
| Sympy es un librería de Python | Usa el lenguaje propio Mathematica|
| Es Software Libre | Es software propietario |

# Por qué usar programas de Matemática Simbólica

- Competencia del siglo xxi
- Pensamiento computacional
- Iniciación a Ciencia de Datos
- Las máquinas calculan mejor que los humanos
- ...

# Por qué usar Sympy

- Es software libre y todo lo que ello conlleva
- Es una biblioteca Python

# Agradecimientos

- A la comunidad Sympy
- A la comunidad Python

```

El fichero empieza con un encabezado *yaml* con los metadatos para *beamer*: Título, fuente, etc.. Luego viene el contenido de la presentación.

Para generar la presentación beamer tendremos que ejecutar el siguiente código:

```
pandoc -s -t beamer input.md -o output.pdf

```

Para más información, consultar la [documentación de pandoc para hacer presentaciones](https://pandoc.org/MANUAL.html#slide-shows)  

**Truco:** Puedes usar el [pandoc online](https://pandoc.org/try/) para generar el fichero beamer y desde un editor online de latex, [overleaf](https://www.overleaf.com/) por ejemplo, generar el pdf.

El código anterior genera el siguiente [documento pdf](https://gitlab.com/crdguez/crdguez.gitlab.io/-/raw/master/content/post_sastre/beamer_con_pandoc/que_es_sympy_gamma.pdf)

