---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Exportando documentos en google docs a markdown"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-09-08T09:33:45+02:00
lastmod: 2020-09-08T09:33:45+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "Photo by Victoria Priessnitz"
  focal_point: ""
  preview_only: false


links:
#  - icon_pack: fab
#   icon: twitter
#   name: Follow
#   url: 'https://twitter.com/Twitter'
# - icon_pack: fab
#  icon: gitlab
#  name: Originally published on Medium
#  url: 'https://medium.com'

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [] 
---

A veces quiero incluir el contenido de un documento que está en Google Docs a una página web. Hoy en día multitud de generadores de páginas web estáticas permitan subir contenido en markdwon.

Por esta razón, he necesitado convertir el documento a markdown. 

Os comento el procedimiento que sigo:

* Descargo el documento en formato *html*
* Descomprimo el *zip* generado en el paso anterior
* Convierto con el fichero *html* que me aparezca a *markdown* usando [pandoc](https://pandoc.org/)

La instrucción de conversión es la siguiente:

```shell
pandoc --from html input.html --to markdown_strict -o output.md
```

