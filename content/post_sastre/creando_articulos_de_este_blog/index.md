---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Creando artículos de este blog"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2022-03-26T07:34:00+01:00
lastmod: 2022-03-26T07:34:00+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Como escribo de ciento en viento, cada vez que tengo que crear un "artículo" nuevo en el blog tengo que irme a la documentación de [wowchwmy](https://wowchemy.com/) para ver cómo lo creo desde un terminal.

La documentación se encuentra exactamente en [https://wowchemy.com/docs/content/blog-posts/](https://wowchemy.com/docs/content/blog-posts/).

En concreto tenemos que ejecutar el siguiente comando desde la raíz del repositorio donde se encuentre nuestra web. En mi caso tengo que lanzar:

```
$ hugo new  --kind post post/my-article-name
```
