---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Función para resolver Sistemas por Gauss"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2021-11-03T07:40:24+01:00
lastmod: 2021-11-03T07:40:24+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Función en Sympy que a partir de la matriz asociada a un sistema la diagonaliza y resuelve:

{{< gist crdguez 6805aac8cdda2ebd4291291d685735ac  >}}


Para manejar sistemas, sus matrices y su escritura en $LaTeX$ he creado las siguientes funciones:

{{< gist crdguez 270896dd5d36ba9b07ba91db8479fbdb  >}}

