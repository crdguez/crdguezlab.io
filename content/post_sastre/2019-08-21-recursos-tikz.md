---
eye_catch: null
mathjax: true
tags:
  - LaTeX
title: Recursos tikz
date: "2019-08-21T00:00:00Z"
---

He encontrado útil los siguientes enlaces de recursos:

   * [https://github.com/xiaohanyu/awesome-tikz](https://github.com/xiaohanyu/awesome-tikz)
   * [https://tex.stackexchange.com/questions/158668/nice-scientific-pictures-show-off](https://tex.stackexchange.com/questions/158668/nice-scientific-pictures-show-off)
   * [fractal de Sierpinski](https://tex.stackexchange.com/questions/386964/tikz-fractal-sierpinski-tiling-arrowhead)

En cuanto a editores gráficos de tikz:
   
- [tikzcd](https://tikzcd.yichuanshen.de/)
- [tikzit ](https://tikzit.github.io/)
