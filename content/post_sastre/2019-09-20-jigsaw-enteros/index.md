---
eye_catch: null
mathjax: true
tags:
  - Enteros
  - ESO 2
  - puzzles

title: "Puzzle de enteros"
subtitle: "Repasa operaciones combinadas con enteros"
date: "2019-09-20T00:00:00Z"
markup: mmark
---

En 2º de la ESO hemos decidido empezar el curso repasando las operaciones con enteros. 

He modificado una de las "fichas de ejercicios" que normalmente utilizamos para que tenga aspecto de puzzle. El recurso generado se compone de tres documentos:

* [piezas del puzzle desordenadas](./enteros3-puzzle.pdf)
* [solución del puzzle](./enteros3-solution.pdf)
* [lista de operaciones y resultados](./enteros3-table.pdf)

Para generar el puzzle he utilizado el fantástico programa abierto [Jigsaw-generator](https://github.com/juliangilbey/jigsaw-generator) de [Julian Gilbey](https://github.com/juliangilbey). En la carpeta *doc* del repositorio de Julian podéis encontrar perfectamente documentado su uso. Sin embargo, en su día, cree un entrada explicando lo principal: [https://crdguez.github.io/2019/01/11/Puzzles_matematicos_jgisaw/](https://crdguez.github.io/2019/01/11/Puzzles_matematicos_jgisaw/)

Espero que a alguien más le resulte útil.