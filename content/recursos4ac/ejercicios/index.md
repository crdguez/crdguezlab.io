---
title: Ejercicios
summary: Colección de ejercicios complementarioas al libro
tags:
- Demo
date: "2016-04-27T00:00:00Z"

weight: 30

# Optional external URL for project (replaces project detail page).
external_link: https://github.com/crdguez/mat4ac/tree/master/ejercicios/build
 

image:
  caption: Photo by Toa Heftiba on Unsplash
  focal_point: Smart
---

