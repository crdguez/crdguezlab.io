---
title: Classrooms
summary: Cursos Virtuales
tags:
- 4eso
date: "2020-09-12T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://github.com/crdguez/mat4ac/tree/master/ejercicios/build

weight: 40

image:
  caption: 
  focal_point: Smart
---

Durante el presente curso se han habilitado las siguientes clases virtuales en google classroom:

* 4 ESO A - grupo 1
* 4 ESO A - grupo 2
* 4 ESO B - grupo 1
* [4 ESO B - grupo 2](https://classroom.google.com/c/MTQ1MzYzNjk4MTEy)
* 4 ESO C - grupo 1
* 4 ESO C - grupo 2

Recuerda que para acceder a tu curso necesitas hacerlo con tu cuenta de google del IES Pedro Cerrada.