---
title: Información Docente
summary: Hoja de Inicio de Curso, Programaciones, etc.
tags:
- 4eso
date: "2020-09-12T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://github.com/crdguez/mat4ac/tree/master/ejercicios/build
 
weight: 10

image:
  caption: Photo  on [Unsplash](https://unsplash.com/)
  focal_point: Smart
---

* {{% staticref "files/inicio_curso_4_academicas.pdf" "newtab" %}}Hoja de Inicio de Curso{{% /staticref %}}
* [Normativa de ESO y Bachillerato](https://educa.aragon.es/web/guest/-/normativa-eso-bachillerato):
	- [Currículo de Matemáticas Académicas](https://educa.aragon.es/documents/20126/521996/42+MATEMATICAS+ORIENTADAS+A+LAS+ENSENANZAS+ACADEMICAS.pdf/ef227457-eb97-f760-0941-e848c04f6346?t=1578923115406)
	- 
* [COVID](https://educa.aragon.es/covid-19)