---
title: Información Docente
summary: Hoja de Inicio de Curso, Programaciones, etc.
tags:
- 1bach
date: "2021-09-01T00:00:00Z"


weight: 10

image:
  caption: Photo  on [Unsplash](https://unsplash.com/)
  focal_point: Smart
---

## Normativa
* [Normativa de ESO y Bachillerato](https://educa.aragon.es/web/guest/-/normativa-eso-bachillerato):
* {{% staticref "files/curriculo_mat_bach.pdf" "newtab" %}}Currículo de matemáticas de Bachillerato{{% /staticref %}}
<!-- * [Normativa de ESO y Bachillerato](https://educa.aragon.es/web/guest/-/normativa-eso-bachillerato):
	- [Currículo de Matemáticas de Bachillerato](https://educa.aragon.es/documents/20126/521996/164+MATEMATICAS+I+y+II.pdf/1a0ad1d6-aaae-0f7e-4b28-65912137e64d?t=1578923271095)
	-
* [COVID](https://educa.aragon.es/covid-19) -->

## Contenidos y programación

* Consultar la {{% staticref "slides/1bach_inicio/" "newtab" %}}información de inicio de curso{{% /staticref %}}


## Recursos

* 
