---
title: 1º Bachillerato Tecnológico
summary: Classroom
tags:
- 1bach
date: "2021-09-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
# external_link: https://github.com/crdguez/mat4ac/tree/master/ejercicios/build

weight: 50

image:
  caption:
  focal_point: Smart

external_link: https://classroom.google.com/c/Mzg4OTg0NzM4NjM5


---
