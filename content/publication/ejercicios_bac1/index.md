---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Ejercicios y soluciones - 1ºCIT"
authors: []
date: 2020-09-03T08:13:55+02:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2020-09-03T08:13:55+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: ""

# Summary. An optional shortened abstract.
summary: ""

tags: [bachillerato1]
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

links:
- icon: github
  icon_pack: fab
  name: notebooks
  url: "https://github.com/crdguez/mat1bac_cit/tree/master/notebooks"

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

Colección, todavía incompleta, de ejercicios y soluciones correspondientes a  **Matemáticas CIT de 1º de Bachillerato en el IES Pedro Cerrada**. 

Recuerda que las soluciones han sido calculadas por un programa y en ocasiones otra expresión equivalente sería más idónea.

{{% alert note %}}
Pinchando arriba en  el icono *pdf* accedes al documento o directamente de [aquí](https://crdguez.gitlab.io/publication/ejercicios_bac1/ejercicios_bac1.pdf)
{{% /alert %}}

Puedes acceder al código fuente que generan las soluciones del documento pinchando arriba en *notebooks*