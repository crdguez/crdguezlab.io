---
draft: true
title: Reunión con padres
summary: 2019-2020 - 2Bachillerato
tags:
- Demo
date: "2016-04-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: "../slides/presentacion_bac2/#/"

image:
  caption: Photo by Toa Heftiba on Unsplash
  focal_point: Smart

slides: presentacion_bac2


---
