---
draft: false
title: Tutoría
summary: Carpeta con contenidos que puedan resultar útiles
tags:
- Demo
date: "2016-09-12T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://drive.google.com/drive/folders/16LB9WXLLBwKkwWcCFXTpAGI-5M19KzBT?usp=sharing

image:
  caption: Photo by NeONBRAND on Unsplash
  focal_point: Smart

slides: bac2_inicio
---